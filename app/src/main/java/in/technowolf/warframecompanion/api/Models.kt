/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.api

import `in`.technowolf.warframecompanion.ui.model.ConstructionProgressModel
import `in`.technowolf.warframecompanion.ui.model.EarthStateModel
import `in`.technowolf.warframecompanion.ui.model.FissuresModel
import `in`.technowolf.warframecompanion.ui.model.PlainsSateModel
import `in`.technowolf.warframecompanion.ui.model.VallisStateModel
import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName

data class PlainsStateRS(
    @SerializedName("id")
    val id: String?,
    @SerializedName("expiry")
    val expiry: String?,
    @SerializedName("activation")
    val activation: String?,
    @SerializedName("isDay")
    val isDay: Boolean?,
    @SerializedName("state")
    var state: String?,
    @SerializedName("timeLeft")
    val timeLeft: String?,
    @SerializedName("isCetus")
    val isCetus: Boolean?,
    @SerializedName("shortString")
    val shortString: String?
) {
    fun toPlainsStateModel() = PlainsSateModel(
        expiry.orEmpty(),
        activation.orEmpty(),
        isDay ?: false,
        state.orEmpty(),
        timeLeft.orEmpty(),
        shortString.orEmpty()
    )
}

@Keep
data class VallisStateRS(
    @SerializedName("id")
    val id: String?,
    @SerializedName("expiry")
    val expiry: String?,
    @SerializedName("isWarm")
    val isWarm: Boolean?,
    @SerializedName("state")
    var state: String?,
    @SerializedName("activation")
    val activation: String?,
    @SerializedName("timeLeft")
    val timeLeft: String?,
    @SerializedName("shortString")
    val shortString: String?
) {
    fun toVallisStateModel() = VallisStateModel(
        expiry.orEmpty(),
        activation.orEmpty(),
        isWarm ?: false,
        state.orEmpty(),
        timeLeft.orEmpty(),
        shortString.orEmpty()
    )
}

@Keep
data class EarthStateRS(
    @SerializedName("id")
    val id: String?,
    @SerializedName("expiry")
    val expiry: String?,
    @SerializedName("activation")
    val activation: String?,
    @SerializedName("isDay")
    val isDay: Boolean?,
    @SerializedName("state")
    var state: String?,
    @SerializedName("timeLeft")
    val timeLeft: String?
) {
    fun toEarthStateModel() = EarthStateModel(
        id.orEmpty(),
        expiry.orEmpty(),
        activation.orEmpty(),
        isDay ?: false,
        state.orEmpty(),
        timeLeft.orEmpty()
    )
}

@Keep
data class ConstructionProgressRS(
    @SerializedName("id")
    val id: String,
    @SerializedName("fomorianProgress")
    val fomorianProgress: String,
    @SerializedName("razorbackProgress")
    val razorbackProgress: String
) {
    fun toConstructionProgressModel(): ConstructionProgressModel {
        val fomorianProgress = fomorianProgress.toFloat().toInt()
        val razorbackProgress = razorbackProgress.toFloat().toInt()
        return ConstructionProgressModel(
            id,
            if (fomorianProgress > 100) 100 else fomorianProgress,
            if (razorbackProgress > 100) 100 else razorbackProgress,
        )
    }
}

@Keep
data class FissuresRS(
    @SerializedName("activation")
    val activation: String?,
    @SerializedName("active")
    val active: Boolean?,
    @SerializedName("enemy")
    val enemy: String?,
    @SerializedName("eta")
    val eta: String?,
    @SerializedName("expired")
    val expired: Boolean?,
    @SerializedName("expiry")
    val expiry: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("missionType")
    val missionType: String?,
    @SerializedName("node")
    val node: String?,
    @SerializedName("startString")
    val startString: String?,
    @SerializedName("tier")
    val tier: String?,
    @SerializedName("tierNum")
    val tierNum: Int?
) {
    fun toFissuresModel() = FissuresModel(
        activation.orEmpty(),
        active ?: false,
        enemy.orEmpty(),
        eta.orEmpty(),
        expired ?: false,
        expiry.orEmpty(),
        id.orEmpty(),
        missionType.orEmpty(),
        node.orEmpty(),
        startString.orEmpty(),
        tier.orEmpty(),
        tierNum ?: -1
    )
}

@Keep
data class SortieRS(
    @SerializedName("activation")
    val activation: String?,
    @SerializedName("active")
    val active: Boolean?,
    @SerializedName("boss")
    val boss: String?,
    @SerializedName("eta")
    val eta: String?,
    @SerializedName("expired")
    val expired: Boolean?,
    @SerializedName("expiry")
    val expiry: String?,
    @SerializedName("faction")
    val faction: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("rewardPool")
    val rewardPool: String?,
    @SerializedName("startString")
    val startString: String?,
    @SerializedName("variants")
    val variants: List<Variant>
) {
    @Keep
    data class Variant(
        @SerializedName("boss")
        val boss: String?,
        @SerializedName("missionType")
        val missionType: String?,
        @SerializedName("modifier")
        val modifier: String?,
        @SerializedName("modifierDescription")
        val modifierDescription: String?,
        @SerializedName("node")
        val node: String?,
        @SerializedName("planet")
        val planet: String?
    )
}

@Keep
data class NightwaveRS(
    @SerializedName("activation")
    val activation: String?,
    @SerializedName("active")
    val active: Boolean?,
    @SerializedName("activeChallenges")
    val activeChallenges: List<ActiveChallenge>,
    @SerializedName("expiry")
    val expiry: String?,
    @SerializedName("id")
    val id: String?,
    @SerializedName("phase")
    val phase: Int?,
    @SerializedName("rewardTypes")
    val rewardTypes: List<String?>?,
    @SerializedName("season")
    val season: Int?,
    @SerializedName("startString")
    val startString: String?,
    @SerializedName("tag")
    val tag: String?
) {
    @Keep
    data class ActiveChallenge(
        @SerializedName("activation")
        val activation: String?,
        @SerializedName("active")
        val active: Boolean?,
        @SerializedName("desc")
        val desc: String?,
        @SerializedName("expiry")
        val expiry: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("isDaily")
        val isDaily: Boolean,
        @SerializedName("isElite")
        val isElite: Boolean,
        @SerializedName("reputation")
        val reputation: Int?,
        @SerializedName("startString")
        val startString: String?,
        @SerializedName("title")
        val title: String?
    )
}
