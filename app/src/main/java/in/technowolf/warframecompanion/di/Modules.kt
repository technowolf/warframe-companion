/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.di

import `in`.technowolf.warframecompanion.data.dashboard.DashboardService
import `in`.technowolf.warframecompanion.data.fissures.FissuresService
import `in`.technowolf.warframecompanion.data.invasion.InvasionService
import `in`.technowolf.warframecompanion.data.nightwave.NightwaveService
import `in`.technowolf.warframecompanion.data.sortie.SortieService
import `in`.technowolf.warframecompanion.data.worldtracker.WorldTrackerService
import `in`.technowolf.warframecompanion.ui.dashboard.DashboardViewModel
import com.chuckerteam.chucker.api.ChuckerCollector
import com.chuckerteam.chucker.api.ChuckerInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit

val retrofitModule = module {
    single { okHttpProvider(get()) }
    single { retrofitProvider(get(), "https://api.warframestat.us/pc/") }
}

val repoModule = module {
    single { get<Retrofit>().create(DashboardService::class.java) }
    single { get<Retrofit>().create(FissuresService::class.java) }
    single { get<Retrofit>().create(InvasionService::class.java) }
    single { get<Retrofit>().create(NightwaveService::class.java) }
    single { get<Retrofit>().create(SortieService::class.java) }
    single { get<Retrofit>().create(WorldTrackerService::class.java) }
}

val viewModelModule = module {
    viewModel { DashboardViewModel(get(), get(), get(), get(), get()) }
}

val appModule = module {
    single {
        ChuckerInterceptor.Builder(get())
            .collector(get())
            .maxContentLength(250000L)
            .alwaysReadResponseBody(false)
            .build()
    }
    single {
        ChuckerCollector(context = get(), showNotification = true)
    }
}
