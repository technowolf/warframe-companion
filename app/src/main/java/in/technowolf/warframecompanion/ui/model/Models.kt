/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.model

data class PlainsSateModel(
    val expiry: String = "",
    val activation: String = "",
    val isDay: Boolean = false,
    var state: String = "",
    val timeLeft: String = "",
    val shortString: String = ""
)

data class VallisStateModel(
    val expiry: String = "",
    val activation: String = "",
    val isWarm: Boolean = false,
    var state: String = "",
    val timeLeft: String = "",
    val shortString: String = ""
)

data class EarthStateModel(
    val id: String = "",
    val expiry: String = "",
    val activation: String = "",
    val isDay: Boolean = false,
    var state: String = "",
    val timeLeft: String = ""
)

data class ConstructionProgressModel(
    val id: String = "",
    val fomorianProgress: Int = 0,
    val razorbackProgress: Int = 0
)

data class FissuresModel(
    val activation: String = "",
    val active: Boolean = false,
    val enemy: String = "",
    var eta: String = "",
    val expired: Boolean = false,
    val expiry: String = "",
    val id: String = "",
    val missionType: String = "",
    val node: String = "",
    val startString: String = "",
    val tier: String = "",
    val tierNum: Int = -1,
    var seconds: Long = 0L
)
