/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.worldtracker

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.ui.core.TimerLiveData
import `in`.technowolf.warframecompanion.ui.dashboard.DashboardViewModel
import `in`.technowolf.warframecompanion.utils.Utility.getSecondsFromTime
import `in`.technowolf.warframecompanion.utils.capitalizeWithLocale
import `in`.technowolf.warframecompanion.utils.loading
import `in`.technowolf.warframecompanion.utils.loadingFinished
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.world_tracker_fragment.srlWorldTracker
import kotlinx.android.synthetic.main.world_tracker_fragment.tvEarthRemainingTime
import kotlinx.android.synthetic.main.world_tracker_fragment.tvEarthState
import kotlinx.android.synthetic.main.world_tracker_fragment.tvPoeRemainingTime
import kotlinx.android.synthetic.main.world_tracker_fragment.tvPoeState
import kotlinx.android.synthetic.main.world_tracker_fragment.tvVallisRemainingTime
import kotlinx.android.synthetic.main.world_tracker_fragment.tvVallisState
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class WorldTrackerFragment : Fragment() {

    private val dashboardViewModel: DashboardViewModel by sharedViewModel()

    private val plainsTimerLiveData = TimerLiveData()
    private val vallisTimerLiveData = TimerLiveData()
    private val earthTimerLiveData = TimerLiveData()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.world_tracker_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        attachObserver()
        srlWorldTracker.loading()
        srlWorldTracker.setOnRefreshListener {
            attachObserver()
        }
    }

    private fun attachObserver() {
        observePlains()
        observeVallis()
        observeEarth()
    }

    private fun observePlains() {
        dashboardViewModel.plainsCycle.observe(viewLifecycleOwner) {
            it.state = it.state.capitalizeWithLocale()
            tvPoeState.text = String.format("Currently it is %s at Plains", it.state)
            startPlainsTimer(it.expiry, it.isDay)
            srlWorldTracker.loadingFinished()
        }
    }

    private fun observeVallis() {
        dashboardViewModel.vallisCycle.observe(viewLifecycleOwner) {
            it.state = it.state.capitalizeWithLocale()
            tvVallisState.text = String.format("Currently it is %s at Orb Vallis", it.state)
            startVallisTimer(it.expiry, it.isWarm)
            srlWorldTracker.loadingFinished()
        }
    }

    private fun observeEarth() {
        dashboardViewModel.earthCycle.observe(viewLifecycleOwner) {
            it.state = it.state.capitalizeWithLocale()
            tvEarthState.text = String.format("Currently it is %s at Earth", it.state)
            startEarthTimer(it.expiry, it.isDay)
        }
    }

    private fun startPlainsTimer(expiry: String, isDay: Boolean) {
        plainsTimerLiveData.totalSeconds = getSecondsFromTime(expiry)
        plainsTimerLiveData.observe(viewLifecycleOwner) {
            tvPoeRemainingTime.text =
                String.format("%s to %s", it.toString(), if (isDay) "Night" else "Day")
        }
    }

    private fun startVallisTimer(expiry: String, isWarm: Boolean) {
        vallisTimerLiveData.totalSeconds = getSecondsFromTime(expiry)
        vallisTimerLiveData.observe(viewLifecycleOwner) {
            tvVallisRemainingTime.text =
                String.format("%s to %s", it.toString(), if (isWarm) "Cold" else "Warm")
        }
    }

    private fun startEarthTimer(expiry: String, isDay: Boolean) {
        earthTimerLiveData.totalSeconds = getSecondsFromTime(expiry)
        earthTimerLiveData.observe(viewLifecycleOwner) {
            tvEarthRemainingTime.text =
                String.format("%s to %s", it.toString(), if (isDay) "Night" else "Day")
        }
    }
}
