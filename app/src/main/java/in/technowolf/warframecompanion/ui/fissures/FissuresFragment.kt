/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.fissures

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.ui.core.FissuresTimerLiveData
import `in`.technowolf.warframecompanion.ui.dashboard.DashboardViewModel
import `in`.technowolf.warframecompanion.ui.model.FissuresModel
import `in`.technowolf.warframecompanion.utils.Utility.getSecondsFromTime
import `in`.technowolf.warframecompanion.utils.loading
import `in`.technowolf.warframecompanion.utils.loadingFinished
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.view.animation.LayoutAnimationController
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_fissures.rvFissures
import kotlinx.android.synthetic.main.fragment_fissures.srlFissures
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class FissuresFragment : Fragment() {

    private val dashboardViewModel: DashboardViewModel by sharedViewModel()

    private val fissuresAdapter = FissuresAdapter(listOf())
    private lateinit var controller: LayoutAnimationController
    private val fissuresTimerLiveData = FissuresTimerLiveData()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_fissures, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {

        controller =
            AnimationUtils.loadLayoutAnimation(requireActivity(), R.anim.layout_animation_fall_down)

        attachObserver()
        setupRecyclerView()

        srlFissures.loading()
        srlFissures.setOnRefreshListener {
            attachObserver()
        }
    }

    private fun attachObserver() {
        dashboardViewModel.fissuresData.observe(viewLifecycleOwner) {
            updateTimers(it)
            rvFissures.layoutAnimation = controller
            rvFissures.scheduleLayoutAnimation()
            srlFissures.loadingFinished()
        }
    }

    private fun updateTimers(fissuresModelList: List<FissuresModel>) {
        fissuresModelList.map { it.seconds = getSecondsFromTime(it.expiry) }
        fissuresTimerLiveData.fissuresList = fissuresModelList
        fissuresTimerLiveData.observe(viewLifecycleOwner) {
            fissuresAdapter.updateFissures(it)
        }
    }

    private fun setupRecyclerView() {
        rvFissures.adapter = fissuresAdapter
    }
}
