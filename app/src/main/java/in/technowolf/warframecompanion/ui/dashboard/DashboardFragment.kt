/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.dashboard

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.utils.loading
import `in`.technowolf.warframecompanion.utils.loadingFinished
import android.animation.ObjectAnimator
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_dashboard.pbFomorianProgress
import kotlinx.android.synthetic.main.fragment_dashboard.pbRazorbackProgress
import kotlinx.android.synthetic.main.fragment_dashboard.srlDashboard
import kotlinx.android.synthetic.main.fragment_dashboard.tvFomorianBuild
import kotlinx.android.synthetic.main.fragment_dashboard.tvRazorbackBuild
import org.koin.androidx.viewmodel.ext.android.sharedViewModel

class DashboardFragment : Fragment() {

    private val dashboardViewModel: DashboardViewModel by sharedViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_dashboard, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        attachObserver()

        srlDashboard.loading()
        srlDashboard.setOnRefreshListener {
            attachObserver()
        }
    }

    private fun attachObserver() {
        dashboardViewModel.constructionProgress.observe(viewLifecycleOwner) {
            tvFomorianBuild.text =
                String.format("Fomorian Build Progress: %d%%", it.fomorianProgress)
            tvRazorbackBuild.text =
                String.format("Razorback Build Progress: %d%%", it.razorbackProgress)
            animateFomorianProgress(it.fomorianProgress)
            animateRazorbackProgress(it.razorbackProgress)
            srlDashboard.loadingFinished()
        }
    }

    private fun animateFomorianProgress(fomorianProgress: Int) {
        ObjectAnimator.ofInt(pbFomorianProgress, "progress", fomorianProgress)
            .setDuration(animationDuration)
            .start()
    }

    private fun animateRazorbackProgress(razorbackProgress: Int) {
        ObjectAnimator.ofInt(pbRazorbackProgress, "progress", razorbackProgress)
            .setDuration(animationDuration)
            .start()
    }

    companion object {
        const val animationDuration = 1000L
    }
}
