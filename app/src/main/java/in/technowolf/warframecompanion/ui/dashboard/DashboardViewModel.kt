/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.dashboard

import `in`.technowolf.warframecompanion.data.dashboard.DashboardService
import `in`.technowolf.warframecompanion.data.fissures.FissuresService
import `in`.technowolf.warframecompanion.data.nightwave.NightwaveService
import `in`.technowolf.warframecompanion.data.sortie.SortieService
import `in`.technowolf.warframecompanion.data.worldtracker.WorldTrackerService
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import kotlinx.coroutines.Dispatchers

class DashboardViewModel(
    private val worldTrackerRepo: WorldTrackerService,
    private val dashboardRepo: DashboardService,
    private val fissuresRepo: FissuresService,
    private val sortieRepo: SortieService,
    private val nightwaveRepo: NightwaveService,
) : ViewModel() {

    fun init() {
        Log.i("TAG", "initializing viewmodel on activity")
    }

    val plainsCycle = liveData(Dispatchers.IO) {
        val retrievedPlainsData = worldTrackerRepo.getCetusCycle()
        emit(retrievedPlainsData.toPlainsStateModel())
    }

    val vallisCycle = liveData(Dispatchers.IO) {
        val retrievedVallisData = worldTrackerRepo.getVallisCycle()
        emit(retrievedVallisData.toVallisStateModel())
    }

    val earthCycle = liveData(Dispatchers.IO) {
        val retrievedEarthData = worldTrackerRepo.getEarthState()
        emit(retrievedEarthData.toEarthStateModel())
    }

    val constructionProgress = liveData(Dispatchers.IO) {
        val retrievedConstructionProgress = dashboardRepo.getConstructionProgress()
        emit(retrievedConstructionProgress.toConstructionProgressModel())
    }

    val fissuresData = liveData(Dispatchers.IO) {
        val retreivedFissuresData = fissuresRepo.getFissures()
        emit(retreivedFissuresData.map { it.toFissuresModel() })
    }

    // TODO: 20/2/20 map the response to presentation model
    val sortieData = liveData(Dispatchers.IO) {
        val retrievedSortieData = sortieRepo.getSortieData()
        emit(retrievedSortieData)
    }

    // TODO: 20/2/20 map the response to presentation model
    val nightwaveData = liveData(Dispatchers.IO) {
        val retrievedNightwaveData = nightwaveRepo.getNightwaveData()
        emit(retrievedNightwaveData)
    }
}
