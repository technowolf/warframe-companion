/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.fissures

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.ui.model.FissuresModel
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_fissures.view.tvFissureNode
import kotlinx.android.synthetic.main.list_item_fissures.view.tvFissureNodeMissionType
import kotlinx.android.synthetic.main.list_item_fissures.view.tvFissureTier
import kotlinx.android.synthetic.main.list_item_fissures.view.tvFissureTimeLeft

class FissuresAdapter(private var fissuresList: List<FissuresModel>) :
    RecyclerView.Adapter<FissuresViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FissuresViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_fissures, parent, false)
        return FissuresViewHolder(itemView)
    }

    override fun getItemCount(): Int = fissuresList.size

    override fun onBindViewHolder(holder: FissuresViewHolder, position: Int) {
        val fissure = fissuresList[position]
        holder.itemView.apply {
            tvFissureNode.text = fissure.node
            tvFissureNodeMissionType.text =
                String.format("%s - %s", fissure.missionType, fissure.enemy)
            tvFissureTier.text = fissure.tier
            tvFissureTimeLeft.text = fissure.eta
        }
    }

    fun updateFissures(fissuresList: List<FissuresModel>) {
        this.fissuresList = fissuresList
        notifyDataSetChanged()
    }
}

class FissuresViewHolder(view: View) : RecyclerView.ViewHolder(view)
