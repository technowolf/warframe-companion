/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.nightwave

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.api.NightwaveRS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_nightwave.view.tvNightwaveChallengeDescription
import kotlinx.android.synthetic.main.list_item_nightwave.view.tvNightwaveChallengeReputation
import kotlinx.android.synthetic.main.list_item_nightwave.view.tvNightwaveChallengeTitle
import kotlinx.android.synthetic.main.list_item_nightwave.view.tvNightwaveChallengeType

class NightwaveAdapter(private var challengeList: List<NightwaveRS.ActiveChallenge>) :
    RecyclerView.Adapter<NightwaveViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NightwaveViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_nightwave, parent, false)
        return NightwaveViewHolder(itemView)
    }

    override fun getItemCount(): Int = challengeList.size

    override fun onBindViewHolder(holder: NightwaveViewHolder, position: Int) {
        val challenge = challengeList[position]
        holder.itemView.apply {
            tvNightwaveChallengeTitle.text = String.format("%d. %s", position + 1, challenge.title)
            tvNightwaveChallengeDescription.text = challenge.desc
            tvNightwaveChallengeReputation.text =
                String.format("Reputation: %d", challenge.reputation)
            tvNightwaveChallengeType.text = when {
                challenge.isDaily -> "Daily Task"
                challenge.isElite -> "Elite Weekly Task"
                else -> "Weekly Task"
            }
        }
    }

    fun updateChallenges(challengeList: List<NightwaveRS.ActiveChallenge>) {
        this.challengeList = challengeList
        notifyDataSetChanged()
    }
}

class NightwaveViewHolder(view: View) : RecyclerView.ViewHolder(view)
