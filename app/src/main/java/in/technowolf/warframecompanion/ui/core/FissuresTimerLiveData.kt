/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package `in`.technowolf.warframecompanion.ui.core

import `in`.technowolf.warframecompanion.ui.model.FissuresModel
import android.os.Handler
import androidx.lifecycle.LiveData

class FissuresTimerLiveData : LiveData<List<FissuresModel>>() {

    private val handler: Handler = Handler()
    private val runnable: Runnable
    private var hours = 0L
    private var minutes = 0L
    private var seconds = 0L
    var fissuresList = listOf<FissuresModel>()

    override fun onActive() {
        super.onActive()
        handler.post(runnable)
    }

    override fun onInactive() {
        super.onInactive()
        handler.removeCallbacks(runnable)
    }

    init {
        runnable = object : Runnable {
            override fun run() {
                fissuresList.map { fissuresModel ->

                    hours = fissuresModel.seconds / 3600
                    minutes = (fissuresModel.seconds % 3600) / 60
                    seconds = fissuresModel.seconds % 60
                    --fissuresModel.seconds

                    fissuresModel.eta = "Time left: " + when {
                        hours > 0L -> "$hours:$minutes:$seconds"
                        minutes > 0L -> "$minutes:$seconds"
                        seconds > 0L -> "$seconds Seconds"
                        seconds < 0L || seconds == 0L -> "Expired."
                        else -> "Operator, There seems some malfunction in app."
                    }
                    fissuresModel
                }.apply {
                    this@FissuresTimerLiveData.value = this
                }
                handler.postDelayed(this, 1000)
            }
        }
    }
}
