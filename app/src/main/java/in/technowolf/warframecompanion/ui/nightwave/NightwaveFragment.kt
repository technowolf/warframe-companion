/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.nightwave

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.ui.dashboard.DashboardViewModel
import `in`.technowolf.warframecompanion.utils.loading
import `in`.technowolf.warframecompanion.utils.loadingFinished
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_nightwave.rvNightwave
import kotlinx.android.synthetic.main.fragment_nightwave.srlNightwave
import kotlinx.android.synthetic.main.fragment_nightwave.tvNightwaveExpiration
import kotlinx.android.synthetic.main.fragment_nightwave.tvNightwaveTitle
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class NightwaveFragment : Fragment() {

    private val dashboardViewModel: DashboardViewModel by sharedViewModel()
    private val nightwaveAdapter = NightwaveAdapter(listOf())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_nightwave, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    private fun init() {
        setupRecyclerView()
        attachObserver()

        srlNightwave.loading()
        srlNightwave.setOnRefreshListener {
            attachObserver()
        }
    }

    private fun attachObserver() {
        dashboardViewModel.nightwaveData.observe(viewLifecycleOwner) {
            tvNightwaveTitle.text = String.format("Nightwave Season: %d", it.season ?: -1)
            tvNightwaveExpiration.text =
                String.format("Expiration: %s", formatDate(it.expiry.orEmpty()))
            nightwaveAdapter.updateChallenges(it.activeChallenges)
            rvNightwave.layoutAnimation =
                AnimationUtils.loadLayoutAnimation(
                    requireActivity(),
                    R.anim.layout_animation_fall_down
                )
            rvNightwave.scheduleLayoutAnimation()
            srlNightwave.loadingFinished()
        }
    }

    private fun setupRecyclerView() {
        rvNightwave.adapter = nightwaveAdapter
    }

    private fun formatDate(dateString: String): String {
        val inputFormatter = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.getDefault())
        val outputFormatter = SimpleDateFormat("dd-MMM-yyy", Locale.getDefault())
        return try {
            outputFormatter.format(inputFormatter.parse(dateString) ?: Date())
        } catch (e: ParseException) {
            e.printStackTrace()
            "Invalid Date"
        }
    }
}
