/*
 * MIT License
 *
 * Copyright (c) 2022. TechnoWolf FOSS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package `in`.technowolf.warframecompanion.ui.sortie

import `in`.technowolf.warframecompanion.R
import `in`.technowolf.warframecompanion.api.SortieRS
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.list_item_sortie.view.tvSortieCondition
import kotlinx.android.synthetic.main.list_item_sortie.view.tvSortieDescription
import kotlinx.android.synthetic.main.list_item_sortie.view.tvSortieMissionType
import kotlinx.android.synthetic.main.list_item_sortie.view.tvSortieNode

class SortieAdapter(private var variants: List<SortieRS.Variant>) :
    RecyclerView.Adapter<SortieViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SortieViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.list_item_sortie, parent, false)
        return SortieViewHolder(itemView)
    }

    override fun getItemCount(): Int = variants.size

    override fun onBindViewHolder(holder: SortieViewHolder, position: Int) {
        val variant = variants[position]
        holder.itemView.apply {
            tvSortieNode.text = variant.node
            tvSortieMissionType.text = variant.missionType
            tvSortieCondition.text = variant.modifier
            tvSortieDescription.text = variant.modifierDescription
        }
    }

    fun updateVariant(variants: List<SortieRS.Variant>) {
        this.variants = variants
        notifyDataSetChanged()
    }
}

class SortieViewHolder(view: View) : RecyclerView.ViewHolder(view)
